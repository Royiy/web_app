from flask import Flask
from .config import Config
from tinydb import TinyDB

app = Flask(__name__)
app.config.from_object(Config)
db = TinyDB('web_app.db')

from app import views

    