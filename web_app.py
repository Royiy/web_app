from app import app

host='0.0.0.0'
port=8080
debug=True


if __name__ == "__main__":
    app.run(host=host, port=port, debug=debug)
    
    
